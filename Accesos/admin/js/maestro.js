$(document).ready(function(){
    
    cargarlogin();
    buscar_datos();

    $('#btnAggMaestro').click(function(){
        var nomM = $('#Mnom').val()
        var apeM = $('#Mape').val()
        var dirM = $('#Mdirec').val()
        var telM = $('#Mtel').val()
        if(nomM.length==0 || apeM.length==0 || dirM.length==0 || telM.length==0){
            alertify.error("Campos Vacios");
        }else{
            $.ajax({
                type: 'POST',
                url: 'Modelos/insert_maestro.php',
                data :{'nomM' : nomM, 'apeM' : apeM, 'dirM' : dirM, 'telM' : telM,}
            })
            .done(function(lista_seccionT){
                alertify.success("Datos Almacenados con Exito");
                buscar_datos();
                cargarlogin();
            })
            .fail(function(){
                alert('Error al Cargar Datos en la tabla')
            })
        }
    })

    $('#btnEditMaestro').click(function(){
        var idM = $('#Eidm').val()
        var nomM = $('#Enomm').val()
        var apeM = $('#Eapem').val()
        var dirM = $('#Edirm').val()
        var telM = $('#Etelm').val()
        var logM = $('#Elogm').val()
        $.ajax({
            type: 'POST',
            url: 'Modelos/update_maestro.php',
            data :{'nomM' : nomM, 'apeM' : apeM, 'dirM' : dirM, 'telM' : telM, 'idM' : idM, 'logM' : logM}
        })
        .done(function(lista_seccionT){
            buscar_datos();
        })
        .fail(function(){
          alert('Error al Cargar las secciones en la tablas')
        })
    })


    function cargarlogin(){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_login.php'
        })
        .done(function(lista_alumno){
            $('#Elogm').html(lista_alumno)
        })
        .fail(function(){
            alert('Error al Cargar los Maestros en el select')
        })
    }
    function buscar_datos(consulta){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_maestroT.php',
            dataType: 'html',
            data: {consulta: consulta}
        })
        .done(function(respuesta){
            $('#Tmaestro').html(respuesta)
        })
        .fail(function(){
            alert('Error al Cargar alumno')
        })
    }

    $(document).on('keyup', '#caja_busqueda', function(){
        var valor=$(this).val();
        if(valor != ""){
            buscar_datos(valor);
        }else{
            buscar_datos();
        }
    })

})

function editMaes(datos){
    
    d=datos.split("||");
    
    
    $('#Eidm').val(d[0]);
    $('#Enomm').val(d[1]);
    $('#Eapem').val(d[2]);
    $('#Edirm').val(d[3]);
    $('#Etelm').val(d[4]);
    $('#Elogm').val(d[5]);
}
