<?php
	class crud{
		
		public function obtenAlumxGra($id_grado){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT * FROM alumno INNER JOIN matricula ON alumno.id_alumno=matricula.id_alumno WHERE matricula.id_grado='$id_grado';";
			$result =mysqli_query($conexion,$sql);
			if(!$result){
				die("Error");
			}else{
				while($data=mysqli_fetch_assoc($result)){
					$arreglo[]=array_map("utf8_encode", $data);
				}
				return $arreglo;
			}
		}

		public function validarAxG($idGrado,$idAlumno){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT IF( EXISTS(SELECT * FROM matricula WHERE id_alumno='$idAlumno' AND id_grado='$idGrado'), 1, 0);";
			$result =mysqli_query($conexion,$sql);
			$ver=mysqli_fetch_row($result);
			$datos=array(
				'result_consulta' =>$ver[0]);
			return $datos;
		}
		
	    public function obtenerEvxPerxAlumno($idProfesor, $idMateria,$numPer){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT evaluaciones.id_evaluacion,evaluaciones.nombre_evaluacion, evaluaciones.porcentaje, graxproxmat.id_grado,materia.materia  FROM evaluaciones INNER JOIN profxmat ON evaluaciones.id_profXmat=profxmat.id_profXmat  INNER JOIN graxproxmat ON graxproxmat.id_profXmat=profxmat.id_profXmat INNER JOIN materia ON materia.id_materia=profxmat.id_materia 
					 WHERE profxmat.id_materia='$idMateria' AND profxmat.id_profesor='$idProfesor' AND evaluaciones.id_periodo='$numPer';";
			$result =mysqli_query($conexion,$sql);
			if(!$result){
				die("Error");
			}else{
				while($data=mysqli_fetch_assoc($result)){
					$arreglo[]=$data;
				}
				return $arreglo;
			}
		}

	    public function validarExisDeNotasAlumSelec($idPeriodo,$idProfXMat,$idAlumno){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT IF( EXISTS(SELECT * FROM nota INNER JOIN evaluaciones ON nota.id_evaluacion=evaluaciones.id_evaluacion INNER JOIN matricula ON matricula.id_matricula=nota.id_matricula WHERE evaluaciones.id_periodo='$idPeriodo' AND evaluaciones.id_profXmat='$idProfXMat' AND matricula.id_alumno='$idAlumno'), 1, 0);";
			$result =mysqli_query($conexion,$sql);
			$ver=mysqli_fetch_row($result);
			$datos=array(
				'result_consulta' =>$ver[0]);
			return $datos;
		}

		public function obtenIdMatriculaXAlumno($idAlumno){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT id_matricula FROM matricula WHERE id_alumno='$idAlumno';";
			$result =mysqli_query($conexion,$sql);
			if(!$result){
				die("Error");
			}else{
				while($data=mysqli_fetch_assoc($result)){
					$arreglo[]=array_map("utf8_encode", $data);
				}
				return $arreglo;
			}
		}

		//Funcion para agregar las notas
		public function agregarNotas($idEvaluacion,$nota,$idMatricula){
			$obj= new conectar();
			$conexion=$obj-> conexion();
			$sql="INSERT INTO nota (id_evaluacion,nota,id_matricula)
				VALUES ('$idEvaluacion','$nota','$idMatricula');";
			return mysqli_query($conexion, $sql);
		}

	  public function obtenerEvaluacionesGlobales1($idProfxMat){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT alumno.nombre_alum, alumno.apellido_alum,alumno.id_alumno FROM alumno INNER JOIN matricula ON alumno.id_alumno=matricula.id_alumno INNER JOIN nota ON nota.id_matricula=matricula.id_matricula INNER JOIN evaluaciones ON evaluaciones.id_evaluacion= nota.id_evaluacion WHERE evaluaciones.id_profXmat='$idProfxMat' GROUP by alumno.nombre_alum;";
			$result =mysqli_query($conexion,$sql);
			if(!$result){
				die("Error");
			}else{
				while($data=mysqli_fetch_assoc($result)){
					$arreglo[]=array_map("utf8_encode", $data);
				}
				return $arreglo;
			}
		}

	   public function obtenerEvxPerxAlumno2($numPer, $idProfXMat,$idAlumno){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT * FROM nota INNER JOIN evaluaciones ON nota.id_evaluacion=evaluaciones.id_evaluacion INNER JOIN matricula ON matricula.id_matricula=nota.id_matricula WHERE evaluaciones.id_periodo='$numPer' AND evaluaciones.id_profXmat='$idProfXMat' AND matricula.id_alumno='$idAlumno';";
			$result =mysqli_query($conexion,$sql);
			if(!$result){
				die("Error");
			}else{
				while($data=mysqli_fetch_assoc($result)){
					$arreglo[]=$data;
				}
				return $arreglo;
			}
		}

















		public function actualizar($datos){
			$obj= new conectar();
			$conexion=$obj->conexion();

			$sql="UPDATE areas SET area='$datos[1]'
									 WHERE id_area='$datos[0]'";
			return mysqli_query($conexion,$sql);
		}

		public function eliminar($idjuego){
			$obj= new conectar();
			$conexion=$obj->conexion();

			$sql="DELETE FROM t_juegos 
			WHERE id_juego='$idjuego'";
			return mysqli_query($conexion,$sql);
		}

	}

?>