<?php
	class Allconsultas
	{
		function mostrarMaterias($con){
			$materias = array();
			$materias['items'] = array();
			$nie=$_SESSION['usuario'];

			$query = "SELECT (mat.materia)as materia,(sum(nt.nota*ev.porcentaje))as notap,(pe.periodo)as periodo from matricula mt 
			    join nota nt on mt.id_matricula=nt.id_matricula 
			    join alumno al on mt.id_alumno=al.id_alumno 
			    join evaluaciones ev on nt.id_evaluacion=ev.id_evaluacion 
			    join periodos pe on ev.id_periodo=pe.id_periodo
			    join profxmat pm on ev.id_profxmat= pm.id_profXmat
			    join materia mat on pm.id_materia= mat.id_materia
			    where al.nie=$nie group by pe.periodo";
			// $query = "SELECT (mat.materia)as materia from graxproxmat gpt
			// join profxmat pm on gpt.id_profxmat=pm.id_profxmat 
			// join materia mat on pm.id_materia=mat.id_materia 
			// join grado gd on gpt.id_grado=gd.id_grado ";

			$resul = mysqli_query($con,$query);

			if (mysqli_affected_rows($con)!=0) {
				while ($row=mysqli_fetch_array($resul,MYSQLI_ASSOC)) {
					array_push($materias['items'], array(
						'materia'=>$row['materia'],
						'notap'=>$row['notap'],
						'periodo'=>$row['periodo']
					));
				}
			}
			return $materias;
// 			select (sum(nt.nota*ev.porcentaje))as notap from nota nt 
// join evaluaciones ev on nt.id_evaluacion=ev.id_evaluacion 
// join periodos pe on ev.id_periodo=pe.id_periodo
		}
	}
?>