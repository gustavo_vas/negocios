$(document).ready(function(){
    cargarSelectMaestro();
    cargartablaMateria();
    cargartablaPxM()

    

    $('#Sprof').on('change', function(){
        var idP = $('#Sprof').val()
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_materia.php',
            data :{'idP' : idP }
        })
        .done(function(lista_seccion){
            $('#Smat').html(lista_seccion)
        })
        .fail(function(){
          alert('Error al Cargar las Secciones')
        })
    })

    $('#btnAggMateria').click(function(){
        var NomM = $('#nommat').val()
        if(NomM.length==0){
            alertify.error("Campos Vacios");
        }else{
            $.ajax({
                type: 'POST',
                url: 'Modelos/insert_materia.php',
                data :{'NomM' : NomM }
            })
            .done(function(lista_gradosT){
                alertify.success("Datos Almacenados con Exito");
                cargartablaMateria();
            })
            .fail(function(){
                alert('Error al Cargar los Grados en las tablas')
            })
        }
    })

    $('#btnAsiganarM').click(function(){
        var Sprof = $('#Sprof').val()
        var Smat = $('#Smat').val()
        $.ajax({
            type: 'POST',
            url: 'Modelos/insert_mxp.php',
            data :{'Sprof' : Sprof , 'Smat' : Smat }
        })
        .done(function(lista_gradosT){
            cargartablaPxM();
        })
        .fail(function(){
          alert('Error al Cargar los Grados en las tablas')
        })
    })

    function cargarSelectMaestro(){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_maestro.php'
        })
        .done(function(lista_maestro){
            $('#Sprof').html(lista_maestro)
        })
        .fail(function(){
            alert('Error al Cargar los Maestros en el select')
        })
    }
    function cargartablaMateria(){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_materiaT.php'
        })
        .done(function(lista_materiaT){
            $('#Tmateria').html(lista_materiaT)
        })
        .fail(function(){
            alert('Error al Cargar las materias en la tabla')
        })
    }

    function cargartablaPxM(){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_pxmT.php'
        })
        .done(function(lista_pxmT){
            $('#Tpxm').html(lista_pxmT)
        })
        .fail(function(){
            alert('Error al Cargar las materias en la tabla')
        })
    }
    
})
