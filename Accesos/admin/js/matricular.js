$(document).ready(function(){
    buscar_datos();
    cargarselectGrado();
    function buscar_datos(consulta){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_alumnoT.php',
            dataType: 'html',
            data: {consulta: consulta}
        })
        .done(function(respuesta){
            $('#datos').html(respuesta)
        })
        .fail(function(){
            alert('Error al Cargar las secciones en la tabla')
        })
    }

    $(document).on('keyup', '#caja_busqueda', function(){
        var valor=$(this).val();
        if(valor != ""){
            buscar_datos(valor);
        }else{
            buscar_datos();
        }
    })

    function cargarselectGrado(){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_grado.php'
        })
        .done(function(lista_pmx){
            $('#Sgrado').html(lista_pmx)
        })
        .fail(function(){
            alert('Error al Cargar los Grados')
        })
    }

    $('#btnMatricular').click(function(){
        var alum = $('#Midalum').val()
        var grado = $('#Sgrado').val()
        $.ajax({
            type: 'POST',
            url: 'Modelos/insert_matricula.php',
            data :{'alum' : alum, 'grado' : grado}
        })
        .done(function(lista_gradoT){
            //cargarTablaGrado();
        })
        .fail(function(){
          alert('Error al Cargar los grados en la tabla1')
        })
    })
    
})


function matri(datos){
    
    d=datos.split("||");
    

    $('#Midalum').val(d[0]);
    $('#Mcodalum').val(d[1]);
    $('#Mnom').val(d[2]);
    $('#Mape').val(d[3]);
    $('#Mdirec').val(d[4]);
    $('#Mtel').val(d[5]);
}

