<?php
	class crud{
		public function agregar($datos){
			$obj= new conectar();
			$conexion=$obj-> conexion();
			$sql="INSERT INTO evaluaciones (id_profXmat,id_periodo,nombre_evaluacion,porcentaje)
				VALUES ('$datos[0]',
						'$datos[1]', '$datos[2]','$datos[3]')";
			return mysqli_query($conexion, $sql);
		}



		//Aqui obtenemos los grados que son impartidos por x profesor
		public function obtenDatosGrados($id_profesor){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT gpm.id_graXproxMat,gpm.id_grado,g.grado FROM grado as g INNER JOIN graXproXmat as gpm ON g.id_grado=gpm.id_grado INNER JOIN profXmat pm ON gpm.id_profXmat=pm.id_profXmat WHERE pm.id_profesor='$id_profesor' GROUP by gpm.id_grado";
			$result =mysqli_query($conexion,$sql);
			// $ver=mysqli_fetch_row($result);
			// $datos=array(
			// 	'id_area' =>$ver[0],
			// 	'area' => $ver[1]);
			//return $datos;

			///
			if(!$result){
				die("Error");
			}else{
				while($data=mysqli_fetch_assoc($result)){
					$arreglo["data"][]=$data;
				}
				return $arreglo;
			}
		}

		public function validarGradoYProfesor($idProfesor, $idGrado){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT IF( EXISTS(
             SELECT gpm.id_graXproxMat,gpm.id_grado FROM graXproXmat as gpm INNER JOIN profXmat pm ON gpm.id_profXmat=pm.id_profXmat WHERE pm.id_profesor='$idProfesor' and gpm.id_grado='$idGrado'), 1, 0);";
			$result =mysqli_query($conexion,$sql);
			$ver=mysqli_fetch_row($result);
			$datos=array(
				'result_consulta' =>$ver[0]);
			return $datos;
		}

		public function obtenMxGxP($id_profesor,$id_grado){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT pxm.id_materia,m.materia FROM materia as m INNER JOIN profXmat as pxm ON m.id_materia=pxm.id_materia INNER JOIN graXproXmat as gpm ON pxm.id_profXmat=gpm.id_profXmat WHERE gpm.id_grado='$id_grado' and pxm.id_profesor='$id_profesor'";
			$result =mysqli_query($conexion,$sql);
			if(!$result){
				die("Error");
			}else{
				while($data=mysqli_fetch_assoc($result)){
					$arreglo[]=$data;
				}
				return $arreglo;
			}
		}

		public function validarMateriaYProfesor($idProfesor, $idMateria){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT IF( EXISTS(SELECT id_profXmat FROM profxmat WHERE id_profesor='$idProfesor' AND id_materia='$idMateria'), 1, 0);";
			$result =mysqli_query($conexion,$sql);
			$ver=mysqli_fetch_row($result);
			$datos=array(
				'result_consulta' =>$ver[0]);
			return $datos;
		}

		    public function validarExisEva($idProfesor, $idMateria,$numPer){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT IF( EXISTS(SELECT evaluaciones.porcentaje FROM evaluaciones INNER JOIN profxmat ON evaluaciones.id_profXmat=profxmat.id_profXmat 
			WHERE profxmat.id_materia='$idMateria' AND profxmat.id_profesor='$idProfesor' AND evaluaciones.id_periodo='$numPer'), 1, 0);";
			$result =mysqli_query($conexion,$sql);
			$ver=mysqli_fetch_row($result);
			$datos=array(
				'result_consulta' =>$ver[0]);
			return $datos;
		}


		public function validarPorcEva($idProfesor, $idMateria,$numPer){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT evaluaciones.porcentaje,evaluaciones.id_profXmat, graxproxmat.id_grado,materia.materia  FROM evaluaciones INNER JOIN profxmat ON evaluaciones.id_profXmat=profxmat.id_profXmat  INNER JOIN graxproxmat ON graxproxmat.id_profXmat=profxmat.id_profXmat INNER JOIN materia ON materia.id_materia=profxmat.id_materia 
					 WHERE profxmat.id_materia='$idMateria' AND profxmat.id_profesor='$idProfesor' AND evaluaciones.id_periodo='$numPer';";
			$result =mysqli_query($conexion,$sql);
			if(!$result){
				die("Error");
			}else{
				while($data=mysqli_fetch_assoc($result)){
					$arreglo[]=$data;
				}
				return $arreglo;
			}
		}

	  public function obtenIdProfXMat($idMateria){
			$obj= new conectar();
			$conexion=$obj->conexion();
			$sql="SELECT profxmat.id_profXmat,materia.materia FROM profxmat INNER JOIN materia ON profxmat.id_materia=materia.id_materia WHERE profxmat.id_materia='$idMateria';";
			$result =mysqli_query($conexion,$sql);
			if(!$result){
				die("Error");
			}else{
				while($data=mysqli_fetch_assoc($result)){
					$arreglo[]=array_map("utf8_encode", $data);
				}
				return $arreglo;
			}
		}



















		public function actualizar($datos){
			$obj= new conectar();
			$conexion=$obj->conexion();

			$sql="UPDATE areas SET area='$datos[1]'
									 WHERE id_area='$datos[0]'";
			return mysqli_query($conexion,$sql);
		}

		public function eliminar($idjuego){
			$obj= new conectar();
			$conexion=$obj->conexion();

			$sql="DELETE FROM t_juegos 
			WHERE id_juego='$idjuego'";
			return mysqli_query($conexion,$sql);
		}

	}

?>