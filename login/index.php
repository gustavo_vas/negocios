<!DOCTYPE html>
<html lang="es">
<head>
    <title>Iniciar Sesion</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
    <style type="text/css">
      .container-login100 {
        background-image: url(images/im2.jpg);
        background-position: center center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
        background-color: #66999;
      }
      .wrap-login100{
         background-color:rgb(0,0,0,0.9);
         border-radius: 15px;
      }
      .login100-form-title, .input100, .focus-input100{
        color: white;
        
      }
    </style>
</head>
<body>
    	<!-- <div id="container" > -->
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
    			<?php include('more-login.php');?>
            </div>
        </div>
    </div>
    <!--===============================================================================================-->
    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
    <script src="js/main.js"></script>
</body>
</html>    	