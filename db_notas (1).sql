-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 01-06-2019 a las 07:44:14
-- Versión del servidor: 5.7.21
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_notas`
--

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `procedure_mostrarNotas`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_mostrarNotas` (IN `pnie` VARCHAR(15))  select nt.nota,pe.periodo from matricula mt 
    join nota nt on mt.id_matricula=nt.id_matricula 
    join alumno al on mt.id_alumno=al.id_alumno 
    join evaluaciones ev on nt.id_evaluacion=ev.id_evaluacion 
    join periodos pe on ev.id_periodo=pe.id_periodo 
    where al.nie=pnie$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

DROP TABLE IF EXISTS `alumno`;
CREATE TABLE IF NOT EXISTS `alumno` (
  `id_alumno` int(11) NOT NULL AUTO_INCREMENT,
  `nie` char(15) NOT NULL,
  `nombre_alum` varchar(30) NOT NULL,
  `apellido_alum` varchar(30) NOT NULL,
  `direccion_alum` varchar(80) NOT NULL,
  `telefono_alum` varchar(9) NOT NULL,
  `estado_alum` tinyint(1) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_alumno`),
  KEY `Alumno_login` (`id_usuario`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`id_alumno`, `nie`, `nombre_alum`, `apellido_alum`, `direccion_alum`, `telefono_alum`, `estado_alum`, `id_usuario`) VALUES
(1, '123456', 'Ana Maricela', 'Lopez Perez', 'Santa Ana', '7890-0980', 1, 1),
(2, '789012', 'Pedro Antonio', 'Castro Guardado', 'Santa Ana', '7654-1234', 1, 2),
(3, '3456789', 'Carolina Elizabeth', 'Moran Melgar', 'Santa Ana', '7654-0987', 1, 5),
(4, '098765', 'Karen Vanessa', 'Castro Lopez', 'Santa Ana', '7654-0987', 1, 6),
(5, '212121', 'laura', 'martinez', 'santa ana', '7890-0000', 1, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evaluaciones`
--

DROP TABLE IF EXISTS `evaluaciones`;
CREATE TABLE IF NOT EXISTS `evaluaciones` (
  `id_evaluacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_profXmat` int(11) NOT NULL,
  `id_periodo` int(11) NOT NULL,
  `nombre_evaluacion` varchar(20) NOT NULL,
  `porcentaje` double(9,4) NOT NULL,
  PRIMARY KEY (`id_evaluacion`),
  KEY `evaluaciones_Periodos` (`id_periodo`),
  KEY `evaluaciones_profXmat` (`id_profXmat`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `evaluaciones`
--

INSERT INTO `evaluaciones` (`id_evaluacion`, `id_profXmat`, `id_periodo`, `nombre_evaluacion`, `porcentaje`) VALUES
(1, 1, 1, 'Evaluacion 1', 0.2000),
(2, 1, 1, 'Evaluacion 2', 0.1500),
(3, 2, 1, 'Evaluacion 3', 0.1500),
(4, 3, 4, 'Examen periodo', 0.5000),
(5, 1, 3, 'evaluacion 5', 0.2000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grado`
--

DROP TABLE IF EXISTS `grado`;
CREATE TABLE IF NOT EXISTS `grado` (
  `id_grado` int(11) NOT NULL AUTO_INCREMENT,
  `grado` varchar(40) NOT NULL,
  PRIMARY KEY (`id_grado`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grado`
--

INSERT INTO `grado` (`id_grado`, `grado`) VALUES
(1, '1 General seccion A'),
(2, '2 Tecnico Seccion B'),
(3, '3 Contador Seccion A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `graxproxmat`
--

DROP TABLE IF EXISTS `graxproxmat`;
CREATE TABLE IF NOT EXISTS `graxproxmat` (
  `id_graXproXmat` int(11) NOT NULL AUTO_INCREMENT,
  `id_profXmat` int(11) NOT NULL,
  `id_grado` int(11) NOT NULL,
  PRIMARY KEY (`id_graXproXmat`),
  KEY `Matricula_profXmat` (`id_profXmat`),
  KEY `graXproXmat_Grado` (`id_grado`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `graxproxmat`
--

INSERT INTO `graxproxmat` (`id_graXproXmat`, `id_profXmat`, `id_grado`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(100) NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `id_tipousuartio` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `login_tipousuario` (`id_tipousuartio`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='tabla para el login';

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`id_usuario`, `usuario`, `contrasena`, `id_tipousuartio`) VALUES
(1, '123456', 'ana', 3),
(2, '789012', 'pedro', 3),
(3, 'juan', 'juan', 2),
(4, 'maria', 'maria', 1),
(5, '3456789', 'caro', 3),
(6, 'general', 'general', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materia`
--

DROP TABLE IF EXISTS `materia`;
CREATE TABLE IF NOT EXISTS `materia` (
  `id_materia` int(11) NOT NULL AUTO_INCREMENT,
  `materia` varchar(30) NOT NULL,
  PRIMARY KEY (`id_materia`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materia`
--

INSERT INTO `materia` (`id_materia`, `materia`) VALUES
(1, 'Ciencias Sociales'),
(2, 'Matematicas'),
(3, 'Lenguaje y Literatura'),
(4, 'Ciencias Naturales'),
(5, 'Ingles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matricula`
--

DROP TABLE IF EXISTS `matricula`;
CREATE TABLE IF NOT EXISTS `matricula` (
  `id_matricula` int(11) NOT NULL AUTO_INCREMENT,
  `id_alumno` int(11) NOT NULL,
  `id_grado` int(11) NOT NULL,
  `fecha_matricula` date NOT NULL,
  PRIMARY KEY (`id_matricula`),
  KEY `grupXalum_Alumno` (`id_alumno`),
  KEY `matricula_Grado` (`id_grado`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `matricula`
--

INSERT INTO `matricula` (`id_matricula`, `id_alumno`, `id_grado`, `fecha_matricula`) VALUES
(1, 1, 1, '2019-05-27'),
(2, 2, 2, '2019-05-27'),
(3, 3, 3, '2019-05-27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nota`
--

DROP TABLE IF EXISTS `nota`;
CREATE TABLE IF NOT EXISTS `nota` (
  `id_nota` int(11) NOT NULL AUTO_INCREMENT,
  `id_evaluacion` int(11) NOT NULL,
  `nota` double(9,4) NOT NULL,
  `id_matricula` int(11) NOT NULL,
  PRIMARY KEY (`id_nota`),
  KEY `Nota_evaluaciones` (`id_evaluacion`),
  KEY `id_matricula` (`id_matricula`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nota`
--

INSERT INTO `nota` (`id_nota`, `id_evaluacion`, `nota`, `id_matricula`) VALUES
(5, 1, 7.0000, 1),
(6, 2, 8.0000, 1),
(7, 3, 6.0000, 1),
(8, 4, 9.0000, 1),
(9, 5, 5.0000, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos`
--

DROP TABLE IF EXISTS `periodos`;
CREATE TABLE IF NOT EXISTS `periodos` (
  `id_periodo` int(11) NOT NULL AUTO_INCREMENT,
  `periodo` int(11) NOT NULL,
  PRIMARY KEY (`id_periodo`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `periodos`
--

INSERT INTO `periodos` (`id_periodo`, `periodo`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profesor`
--

DROP TABLE IF EXISTS `profesor`;
CREATE TABLE IF NOT EXISTS `profesor` (
  `id_profesor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `direccion` varchar(80) NOT NULL,
  `telefono` varchar(9) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id_profesor`),
  KEY `Profesor_login` (`id_usuario`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profesor`
--

INSERT INTO `profesor` (`id_profesor`, `nombre`, `apellido`, `direccion`, `telefono`, `id_usuario`) VALUES
(1, 'Juan Alberto', 'Martinez Perez', 'Ahuachapan', '7456-0987', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profxmat`
--

DROP TABLE IF EXISTS `profxmat`;
CREATE TABLE IF NOT EXISTS `profxmat` (
  `id_profXmat` int(11) NOT NULL AUTO_INCREMENT,
  `id_profesor` int(11) NOT NULL,
  `id_materia` int(11) NOT NULL,
  PRIMARY KEY (`id_profXmat`),
  KEY `profXmat_Materia` (`id_materia`),
  KEY `profXmat_Profesor` (`id_profesor`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `profxmat`
--

INSERT INTO `profxmat` (`id_profXmat`, `id_profesor`, `id_materia`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipousuario`
--

DROP TABLE IF EXISTS `tipousuario`;
CREATE TABLE IF NOT EXISTS `tipousuario` (
  `id_tipousuartio` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_usuario` varchar(100) NOT NULL,
  PRIMARY KEY (`id_tipousuartio`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='aloja los tipos de usuario';

--
-- Volcado de datos para la tabla `tipousuario`
--

INSERT INTO `tipousuario` (`id_tipousuartio`, `tipo_usuario`) VALUES
(1, 'Administrador'),
(2, 'Profesor'),
(3, 'Alumno'),
(4, 'General');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
