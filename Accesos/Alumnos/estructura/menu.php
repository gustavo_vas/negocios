 <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="theme-assets/images/backgrounds/02.jpg">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">       
          <li class="nav-item mr-auto"><a class="navbar-brand" href="index.php"><img class="brand-logo" alt="Chameleon admin logo" src="theme-assets/images/logo/logo.png"/>
              <h3 class="brand-text">Estudiante</h3></a></li>
          <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
      </div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
          <li class="active"><a href="index.php"><i class="ft-home"></i><span class="menu-title" data-i18n="">Inicio</span></a>
          </li>
          <li class=" nav-item"><a href="miperfil.php"><i class="ft-user"></i><span class="menu-title" data-i18n="">Mi Perfil</span></a>
          </li>
          <li class=" nav-item"><a href="notas.php"><i class="ft-file-text"></i><span class="menu-title" data-i18n="">Notas</span></a>
          </li>
          <!-- <li class=" nav-item"><a href="historial.php"><i class="ft-folder"></i><span class="menu-title" data-i18n="">Historial</span></a>
          </li> -->
          <li class=" nav-item"><a href="logout.php"><i class="ft-power"></i><span class="menu-title" data-i18n="">Cerrar Sesion</span></a>
          </li>
        </ul>
      </div>
      <div class="navigation-background"></div>
    </div>