<?php
	require_once '../../config/conexion.php';
	//require_once 'Modelos/aggAlum.php';
	$conec=new conexion();
	$con=$conec->conectar();
  //$conec->page_protect();
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Matricular</title>
    <link rel="apple-touch-icon" href="../theme-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="theme-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="../theme-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN CHAMELEON  CSS-->
    <link rel="stylesheet" type="text/css" href="../theme-assets/css/app-lite.css">
    <!-- END CHAMELEON  CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="../theme-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="../theme-assets/css/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <!-- END Custom CSS-->
  </head>
  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    <?php include("estructura/cabecera.php"); ?>

    <?php include("estructura/menu.php"); ?>

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
          <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Matricular</h3>
          </div>
          <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
              <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.html">Inicio</a>
                  </li>
                  <li class="breadcrumb-item active">Matricular
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>


<!-- Matricular Alumno-->
<div class="content-body"><!-- Bar charts section start -->
      <section id="chartjs-bar-charts">
  
        <div class="row">
          <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Matricular Alumno</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-plus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse">
                    <div class="card-body">
                      <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                          <label for="caja_busqueda">Buscar:</label>
                          <input type="text" class="form-control" id="caja_busqueda" name="caja_busqueda" placeholder="Buscar Alumno">
                        </div>
                        
                      </div>                           
                      <div class="height" id="datos">
                              
                      </div>
                    </div>
                </div>
            </div>
        </div>
      </div>  
    </section>
</div>
      </div>
    </div>
    
    <?php include("estructura/footer.php"); ?>

    <!-- MODAL PARA matricular Alumno -->
  <div id="Mmaalum" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">                 
                    <div class="modal-header">            
                      <h4 class="modal-title">Matricular Alumno</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>                    
                    <div class="modal-body">
                      
                        <div class="form-group">
                          <label>Id alumno</label>
                          <input type="text" id="Midalum" name="Midalum" class="form-control" readonly >
	                        
                        </div>
                        <div class="form-group">
                          <label>Codigo Alumno</label>
                          <input type="text" id="Mcodalum" name="Mcodalum" class="form-control" readonly >
                          
                        </div><div class="form-group">
                          <label>Nombre</label>
                          <input type="text" id="Mnom" name="Mnom" class="form-control" readonly >
	                        
                        </div>
                        <div class="form-group">
                          <label>Apellido</label>
                          <input type="text" id="Mape" name="Mape" class="form-control" readonly>
                          
                        </div><div class="form-group">
                          <label>Direccion</label>
                          <input type="text" id="Mdirec" name="Mdirec" class="form-control" readonly >
	                        
                        </div>
                        <div class="form-group">
                          <label>Telefono</label>
                          <input type="text" id="Mtel" name="Mtel" class="form-control" readonly >
                          
                        </div>
                        <div class="form-group">
                          <label>Grado Matricular</label>
                          <select class="form-control" id="Sgrado" name="Sgrado">
                          </select>
                        </div>
                                                  
                    </div>
                    <div class="modal-footer">
                      <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                      <button  id="btnMatricular" class="btn btn-success" data-dismiss="modal">Agregar</button>
                      
                    </div>
                  
                </div>
              </div>
  </div>
    <!-- BEGIN VENDOR JS-->
    <script src="../theme-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="../theme-assets/vendors/js/charts/chart.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CHAMELEON  JS-->
    <script src="../theme-assets/js/core/app-menu-lite.js" type="text/javascript"></script>
    <script src="../theme-assets/js/core/app-lite.js" type="text/javascript"></script>
    <!-- END CHAMELEON  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="../theme-assets/js/scripts/charts/chartjs/bar/column.js" type="text/javascript"></script>
    <script src="../theme-assets/js/scripts/charts/chartjs/bar/bar.js" type="text/javascript"></script>
    <script src="../theme-assets/js/scripts/charts/chartjs/line/line.js" type="text/javascript"></script>
    <script src="../theme-assets/js/scripts/charts/chartjs/pie-doughnut/pie-simple.js" type="text/javascript"></script>
    <script src="../theme-assets/js/scripts/charts/chartjs/pie-doughnut/doughnut-simple.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/matricular.js"></script>
   
  </body>
</html>