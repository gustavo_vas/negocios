$(document).ready(function(){
    cargarselectGrado()
    cargarTablaGrado();

    $.ajax({
        type: 'POST',
        url: 'Modelos/cargar_grado.php'
    })
    .done(function(lista_grado){
        $('#Sgrado').html(lista_grado)
    })
    .fail(function(){
        alert('Error al Cargar los Datos')
    })

    $('#btnAggGrado').click(function(){
        var NomG = $('#aggGra').val()
        if(NomG.length==0){
            alertify.error("Campos Vacios");
        }else{
            $.ajax({
                type: 'POST',
                url: 'Modelos/insert_grado.php',
                data :{'NomG' : NomG}
            })
            .done(function(lista_gradoT){
                alertify.success("Datos Almacenados con Exito");
                cargarTablaGrado();
            })
            .fail(function(){
                alert('Error al Cargar los grados en la tabla1')
            })
        }
    })

    $('#Sgrado').on('change', function(){
        var idPxM = $('#Spxm').val()
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_pxm.php',
            data :{'idPxM' : idPxM }
        })
        .done(function(lista_grado){
            $('#Spxm').html(lista_grado)
        })
        .fail(function(){
          alert('Error al Cargar las Secciones')
        })
    })
    
    $('#btnAggMatAsigGrado').click(function(){
        var pxm = $('#Spxm').val()
        var grado = $('#Sgrado').val()
        $.ajax({
            type: 'POST',
            url: 'Modelos/insert_matasigGrado.php',
            data :{'pxm' : pxm , 'grado' : grado }
        })
        .done(function(lista_seccionT){
            //cargartablaGrupo();
        })
        .fail(function(){
          alert('Error al Cargar las secciones en la tablas')
        })
    })

    function cargarTablaGrado(){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_gradoT.php'
        })
        .done(function(lista_gradoT){
            $('#Tgrado').html(lista_gradoT)
        })
        .fail(function(){
            alert('Error al Cargar los grados en la tabla2')
        })
    }
   
    function cargartablaMatasig(){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_matasig.php'
        })
        .done(function(lista_grupo){
            $('#Tgrupo').html(lista_grupo)
        })
        .fail(function(){
            alert('Error al Cargar las secciones en la tabla')
        })
    }

    function cargarselectGrado(){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_grado.php'
        })
        .done(function(lista_pmx){
            $('#Sgrado').html(lista_pmx)
        })
        .fail(function(){
            alert('Error al Cargar los Grados')
        })
    }

    

    

})