<?php
  require_once '../../config/conexion.php';
  require_once 'Modelos/consultas.php';
  $conec=new conexion();
  $con=$conec->conectar();
  $conec->page_protect();
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <title>Mi Perfil</title>
    <?php include("estructura/enhead.php"); ?>
  </head>
  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
    <!-- datos del usuario ingresado -->
    <?php include("estructura/cabecera.php"); ?>
    <!-- muestra el menu de navegacion -->
    <?php include("estructura/menu.php"); ?>

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
          <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Notas Parciales</h3>
          </div>
          <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
              <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.php">Inicio</a>
                  </li>
                  <li class="breadcrumb-item active">Notas Parciales
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Bar charts section start -->
          <section id="chartjs-bar-charts">
              <!-- Column Chart -->
              <div class="row">
                  <div class="col-12">
                      <div class="card" >
                          <div class="card-header">
                              <h4 class="card-title">Notas Parciales</h4>
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                  <ul class="list-inline mb-0">
                                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                                  </ul>
                              </div>
                          </div>
                          <div class="card-content collapse show">
                              <div class="card-body">
                                  <!-- Table head options start -->
                                  <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover w-auto" style="text-align: center;">
                                      <thead class="thead-dark">
                                        <tr>                                          
                                          <th>Materia</th>
                                          <th>Periodo</th>
                                          <th>Promedio</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                         <?php
                                          $nie=$_SESSION['usuario'];

                                                $query = "SELECT (mat.materia)as materia,(sum(nt.nota*ev.porcentaje))as notap,(pe.periodo)as periodo from matricula mt 
                                                    join nota nt on mt.id_matricula=nt.id_matricula 
                                                    join alumno al on mt.id_alumno=al.id_alumno 
                                                    join evaluaciones ev on nt.id_evaluacion=ev.id_evaluacion 
                                                    join periodos pe on ev.id_periodo=pe.id_periodo
                                                    join profxmat pm on ev.id_profxmat= pm.id_profXmat
                                                    join materia mat on pm.id_materia= mat.id_materia
                                                    where al.nie=$nie and YEAR(mt.fecha_matricula)=YEAR(CURDATE())
                                                    group by mat.materia,pe.periodo";
                                                $resul = mysqli_query($con,$query);

                                                if (mysqli_affected_rows($con)!=0) {
                                                  while ($row=mysqli_fetch_array($resul,MYSQLI_ASSOC)) {
                                                    echo "<tr><td>".$row['materia']."</td>
                                                    <td> Periodo ".$row['periodo']."</td>
                                                    <td>".$row['notap']."</td>
                                                    </tr>";
                                                  }
                                                }
                                        ?>
                                      </tbody>
                                    </table>
                                  </div>
                          <!-- Table head options end -->                                  
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
          <!-- // Bar charts section end -->
        </div>
      </div>
    </div>
    <?php include("estructura/script.php"); ?>
  </body>
</html>