<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Portal del Maestro</title>
    <link rel="apple-touch-icon" href="theme-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="theme-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN CHAMELEON  CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/app-lite.css">
    <!-- END CHAMELEON  CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="theme-assets/css/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <!-- END Custom CSS-->

    <link rel="stylesheet" type="text/css" href="librerias/datatable/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
  <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="librerias/fontawesome/css/font-awesome.css">
  </head>
  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    <!-- fixed-top-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="collapse navbar-collapse show" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
              <li class="nav-item d-block d-md-none"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
              <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
              <li class="nav-item dropdown navbar-search"><a class="nav-link dropdown-toggle hide" data-toggle="dropdown" href="#"><i class="ficon ft-search"></i></a>
                <ul class="dropdown-menu">
                  <li class="arrow_box">
                    <form>
                      <div class="input-group search-box">
                        <div class="position-relative has-icon-right full-width">
                          <input class="form-control" id="search" type="text" placeholder="Search here...">
                          <div class="form-control-position navbar-search-close"><i class="ft-x">   </i></div>
                        </div>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
            </ul>
            
          </div>
        </div>
      </div>
    </nav>



    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="theme-assets/images/backgrounds/02.jpg">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">       
          <li class="nav-item mr-auto"><a class="navbar-brand" ><img class="brand-logo" alt="Chameleon admin logo" src="theme-assets/images/logo/logo.png"/>
              <h3 class="brand-text">Chameleon</h3></a></li>
          <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
      </div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
          <li class=" nav-item"><a href="vistaMisMaterias.php"><i class="ft-home"></i><span class="menu-title" data-i18n="">Regresar</span></a>
          </li>
         
        </ul>
      </div>
    </div>

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
          <div class="content-header-left col-md-4 col-12 mb-2">
            <?php 
            if(empty($_POST['idMateria'])){
             ?>
            <h3 class="content-header-title">Configuración de Materia</h3>
          <?php } else{ ?>
            <h3 class="content-header-title">Configuración de la Materia <?php echo $_POST['nombreMateria']; ?> </h3>
          
          <?php  
          } ?>
          </div>
          <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
              <div class="breadcrumb-wrapper mr-1">

              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- ../../../theme-assets/images/carousel/22.jpg -->

<!-- Header footer section start -->
<section id="header-footer">
	<div class="row match-height" id="miEvaluaciones">
    <?php 
    if(empty($_POST['idMateria'])){
      ?>
      <div class="col-lg-3 col-md-7">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title"><h5>Debe seleccionar una materia</h5></h4>
            <h6 class="card-subtitle text-muted"></h6>            
          </div>
        </div>      
      </div>
      <?php
      }else{
        $idMateria  = $_POST['idMateria'];
        require_once "clases/conexion.php";
        $obj=new conectar();
        $conexion=$obj->conexion();
        $sql="SELECT *  FROM periodos";
        $result=mysqli_query($conexion,$sql);
        while ($mostrar=mysqli_fetch_row($result)) {
          $idPeriodo=$mostrar[1];
          $idProfxMat =$_POST['idProfxMat'];
        ?>

        <div class="col-lg-6 col-md-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Periodo: <?php echo $mostrar[1] ?></h4>
              <h6 class="card-subtitle text-muted"></h6>
            </div>
            <div class="card-body">
              <?php 
              require_once "clases/conexion.php";
              $obj2=new conectar();
              $conexion2=$obj2->conexion();
              $sql2="SELECT id_evaluacion,id_profXmat,id_periodo,nombre_evaluacion,porcentaje FROM evaluaciones where id_periodo=$idPeriodo AND id_profXMat=$idProfxMat";

              $result2=mysqli_query($conexion2,$sql2);
              ?>
                <div class="table-responsive" id="mitabla">
                    <table class="table table-hover table-condensed table-bordered" id="iddatatableEvaluacion<?php echo $mostrar[1] ?>">
                      <thead style="background-color: #2980b9 ; color:white; font-weight: bold;">
                        <tr>
                          <td style="width:50px">Id Periodo</td>
                          <td>Evaluacion</td>
                          <td>Porcentaje</td> 
                          
                        </tr>
                      </thead>
                      
                      <tbody>
                        <?php
                        $contador=0;
                        $porcentajeUti=0.0;
                        $porcentajeTotal= 1.0;
                        while ($mostrar2=mysqli_fetch_row($result2)) {

                          ?>
                          <tr>
                            <td><?php echo $mostrar2[2] ?></td>
                            <td><?php echo $mostrar2[3] ?></td>
                            <td><?php echo $mostrar2[4] ?></td>

                          </tr>

                          <?php 
                          $contador=$contador+1;
                          $porcentajeUti=$porcentajeUti+$mostrar2[4];
                        } ?>
                      </tbody>
                    </table>
                  </div>
            </div>            
            <div class="card-footer border-top-blue-grey border-top-lighten-5 text-muted">
              
              <ul class="nav navbar-nav float-left"> 
              <li class="dropdown dropdown-notification nav-item"><a class="nav-link nav-link-label" data-toggle="dropdown"><h5>Detalles del Periodo</h5></a>
                <div class="dropdown-menu dropdown-menu-center">
                  <div class="arrow_box_right">
                    <a class="dropdown-item" >Cantidad de Evaluaciones: <?php echo $contador ?></a>
                    <a class="dropdown-item" >Porcentaje Utilizado: <?php echo $porcentajeUti ?> - <?php echo $porcentajeUti*100 ?>%</a>
                    <a class="dropdown-item" >Porcentaje Disponible: <?php echo $porcentajeTotal- $porcentajeUti ?> - <?php echo ($porcentajeTotal- $porcentajeUti)*100 ?>%</a>
                  </div>
                </div>
              </li></ul>
              <span class="float-right" data-toggle="modal" data-target="#addNuevaEvaModal" onclick="f_nuevaEvaluacion('<?php echo $idProfxMat ?>','<?php echo $mostrar[1] ?>','<?php echo $porcentajeTotal-$porcentajeUti ?>','<?php echo $contador ?>')" >
                <a  class="card-link">Agregar
                  <i class="la la-angle-right"></i>
                </a>
              </span>
            </div>
          </div>      
        </div>

      <?php
    }
      }
      ?>
		

	</div>
</section>
<!-- Header footer section End -->
          <!-- MODAL PARA AGREGAR UNA NUEVA EVALUACION  -->
            <div id="addNuevaEvaModal" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">                 
                    <div class="modal-header">            
                      <h4 class="modal-title">Agregar Evaluación</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>                    
                    <div class="modal-body">
                      <form id="frmnuevaEv">
                        <div class="form-group">
                          <label>ID periodo:</label>
                          <input type="text" id="idPeriodo" name="idPeriodo" class="form-control" disabled="true">
                        </div>
                        <div class="form-group">
                          <label>ID Profesor por Materia:</label>
                          <input type="text" id="idProfxMat" name="idProfxMat" class="form-control" disabled="true">
                        </div>
                         <div class="form-group">
                          <label>Evaluaciones Disponibles:</label>
                          <input type="text" id="evaDisponibles" name="evaDisponibles" class="form-control" disabled="true">
                        </div>
                        <div class="form-group">
                          <label>Nombre de la Evaluación:</label>
                          <input type="text" id="nombreNuevaEv" name="nombreNuevaEv" class="form-control" required>
                        </div>
                        <div class="form-group">
                          <label>Porcentaje Disponible:</label>
                          <input type="text" id="porcDisponible" name="porcDisponible" class="form-control" required disabled="true">
                        </div> 
                        <div class="form-group">
                          <label>Porcentaje:</label>
                          <input type="number" min="0"  id="porcNuevaEv" name="porcNuevaEv" class="form-control" required >
                        </div>                    
                      </form>                              
                    </div>
                    <div class="modal-footer">
                      <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                      <input type="submit" id="btnAgregarNuevaEv" class="btn btn-success" value="Agregar">
                    </div>
                  
                </div>
              </div>
            </div>

        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-light navbar-border navbar-shadow">

    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="theme-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CHAMELEON  JS-->
    <script src="theme-assets/js/core/app-menu-lite.js" type="text/javascript"></script>
    <script src="theme-assets/js/core/app-lite.js" type="text/javascript"></script>
    <!-- END CHAMELEON  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
<script type="text/javascript" src="librerias/jquery.min.js"></script>
<script type="text/javascript" src="librerias/bootstrap/popper.min.js"></script>
<script type="text/javascript" src="librerias/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="librerias/datatable/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="librerias/alertify/alertify.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#iddatatableEvaluacion1').DataTable();
    $('#iddatatableEvaluacion2').DataTable();
    $('#iddatatableEvaluacion3').DataTable();
    $('#iddatatableEvaluacion4').DataTable();
  });
</script>
<script type="text/javascript">
 
  function f_nuevaEvaluacion(idProfxMateria,idPeriodo,porcDisponible,contEvaluaciones){
    $('#idPeriodo').val(idPeriodo);
    $('#idProfxMat').val(idProfxMateria);
    $('#porcDisponible').val(porcDisponible*100);
    $('#porcNuevaEv').attr('max', porcDisponible*100);
    $('#evaDisponibles').val(5-contEvaluaciones);
    //Recargar la pagina
    //location.reload();

    };

</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btnAgregarNuevaEv').click(function(){
      datos=$('#frmnuevaEv').serialize();
      var cantEvaDisponibles;
      var miPorc;
      var porcDispo;
      var nombreEv;
      cantEvaDisponibles=$("#evaDisponibles").val();
      miPorc=$("#porcNuevaEv").val();
      porcDispo=$("#porcDisponible").val();
      nombreEv=$("#nombreNuevaEv").val();
  //alert('HOLA');
  //alertify.warning(miPorc);
      if($('#nombreNuevaEv').val()==""){
            alertify.warning("¡Debe ingresar el nombre de la nueva Evaluación!");
      }else{
        if($('#porcNuevaEv').val()==""){
            alertify.warning("¡Debe ingresar el porcentaje de la nueva Evaluación!");
        }else{
          if(parseInt(cantEvaDisponibles)<=0 || parseInt(cantEvaDisponibles)>5){
            alertify.warning("¡Solo es permitido un maximo de 5 evaluaciones!");
          }else{
            if(miPorc<0){
              alertify.warning("No se aceptan porcentajes negativos");
            }else{
               if(parseInt(miPorc)>parseInt(porcDispo)){              
              alertify.warning("No hay suficiente porcentaje Disponible");
            }else{
/*                alertify.warning("Todo bien");
                alertify.warning(porcDispo);
                alertify.warning(miPorc);*/
                $.ajax({
                  type:"POST",
                  data:{idProfxMat:$('#idProfxMat').val(), idPeriodo:$('#idPeriodo').val(), nombreNuevaEv:$('#nombreNuevaEv').val(), porcNuevaEv:miPorc/100},
                  url:"procesos_evaluacion/agregar_evaluacion.php",
                  success:function(r){
                    if(r==1){
                      $('#frmnuevaEv')[0].reset();
                      location.reload();
                      $('#addNuevaEvaModal').modal('hide');
                      alertify.success("¡Evaluación agregada con exito!");
                    }else{
                      alertify.error("¡Fallo al agregar Evaluación!");
                    }
                  }
                });









            }
            }
          }
        }
      }

    });
  });
</script>
  </body>
</html>