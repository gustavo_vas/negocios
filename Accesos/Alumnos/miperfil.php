<?php
  require_once '../../config/conexion.php';
  $conec=new conexion();
  $con=$conec->conectar();
  $conec->page_protect();
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <title>Mi Perfil</title>
    <?php include("estructura/enhead.php"); ?>
  </head>
  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">
    <!-- datos del usuario ingresado -->
    <?php include("estructura/cabecera.php"); ?>
    <!-- muestra el menu de navegacion -->
    <?php include("estructura/menu.php"); ?>

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
          <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Mi Perfil</h3>
          </div>
          <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
              <div class="breadcrumb-wrapper mr-1">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="index.php">Inicio</a>
                  </li>
                  <li class="breadcrumb-item active">Mi Perfil
                  </li>
                </ol>
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Bar charts section start -->
          <section id="chartjs-bar-charts">
              <!-- Column Chart -->
              <div class="row">
                  <div class="col-12">
                      <div class="card">
                          <div class="card-header">
                              <h4 class="card-title">Mi Perfil</h4>
                              <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                              <div class="heading-elements">
                                  <ul class="list-inline mb-0">
                                      <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                      <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                      <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                      <li><a data-action="close"><i class="ft-x"></i></a></li>
                                  </ul>
                              </div>
                          </div>
                          <div class="card-content collapse show">
                              <div class="card-body">
                                  <div class="form-group row">
                                      <label for="codAlum" class="col-sm-2 col-form-label">Codigo</label>
                                      <input type="text" class="form-control col-sm-6" id="codAlum" name="codAlum" readonly="readonly" placeholder="Codigo de Estudiante" value="<?php echo $nie; ?>">
                                  </div>
                                  <div class="form-group row">
                                      <label for="apeAlum" class="col-sm-2 col-form-label">Apellidos</label>
                                      <input type="text" class="form-control col-sm-6" id="apeAlum" name="apeAlum" readonly="readonly" placeholder="Apellidos del Estudiante" value="<?php echo $alumApe; ?>">
                                  </div>
                                  <div class="form-group row">
                                      <label for="nomAlum" class="col-sm-2 col-form-label">Nombre</label>
                                      <input type="text" class="form-control col-sm-6" id="nomAlum" name="nomAlum" readonly="readonly" placeholder="Nombre del Estudiante" value="<?php echo $alum; ?>">
                                  </div>
                                  <div class="form-group row">
                                      <label for="gAlum" class="col-sm-2 col-form-label">Grado</label>
                                      <input type="text" class="form-control col-sm-6" id="gAlum" name="gAlum" readonly="readonly" placeholder="Grado del Estudiante" value="<?php echo $grado; ?>">
                                  </div>
                                  <form action="actualizardatos.php" method="post">
                                    <div class="form-group row">
                                        <label for="telAlum" class="col-sm-2 col-form-label">Telefono</label>
                                        <input type="text" class="form-control col-sm-6" id="telAlum" name="telAlum" placeholder="Telefono del Estudiante" value="<?php echo $alumTel; ?>">
                                    </div>
                                    <div class="form-group row">
                                        <label for="conAlum" class="col-sm-2 col-form-label">Contraseña</label>
                                        <input type="password" class="form-control col-sm-6" id="conAlum" name="conAlum" placeholder="Contraseña del Estudiante">
                                    </div>
                                    <div class="form-group row">
                                        <label for="repconAlum" class="col-sm-2 col-form-label">Confirmar Contraseña</label>
                                        <input type="password" class="form-control col-sm-6" id="repconAlum" name="repconAlum" placeholder="Confirmar Contraseña del Estudiante">
                                    </div>
                                    <div class="form-group row">                                    
                                        <label for="repconAlum" class="col-sm-2 col-form-label"></label>
                                        <button type="submit" class="btn btn-primary btn-md">Actualizar Datos</button>                                    
                                    </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
          <!-- // Bar charts section end -->
        </div>
      </div>
    </div>
    <?php include("estructura/script.php"); ?>
  </body>
</html>