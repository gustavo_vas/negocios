<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Portal del Maestro</title>
    <link rel="apple-touch-icon" href="theme-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="theme-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN CHAMELEON  CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/app-lite.css">
    <!-- END CHAMELEON  CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="theme-assets/css/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <!-- END Custom CSS-->
  <link rel="stylesheet" type="text/css" href="librerias/datatable/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
  <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="librerias/fontawesome/css/font-awesome.css">
  </head>
  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    <!-- fixed-top-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="collapse navbar-collapse show" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
              <li class="nav-item d-block d-md-none"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
              <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
              <li class="nav-item dropdown navbar-search"><a class="nav-link dropdown-toggle hide" data-toggle="dropdown" href="#"><i class="ficon ft-search"></i></a>
                <ul class="dropdown-menu">
                  <li class="arrow_box">
                    <form>
                      <div class="input-group search-box">
                        <div class="position-relative has-icon-right full-width">
                          <input class="form-control" id="search" type="text" placeholder="Search here...">
                          <div class="form-control-position navbar-search-close"><i class="ft-x">   </i></div>
                        </div>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
            </ul>

          </div>
        </div>
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="theme-assets/images/backgrounds/02.jpg">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">       
          <li class="nav-item mr-auto"><a class="navbar-brand" ><img class="brand-logo" alt="Chameleon admin logo" src="theme-assets/images/logo/logo.png"/>
              <h3 class="brand-text">Chameleon</h3></a></li>
          <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
      </div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class=" nav-item"><a href="vistaMisGrados.php"><i class="ft-home"></i><span class="menu-title" data-i18n="">Regresar</span></a>
          </li>
        </ul>
      </div>
    </div>

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
          <div class="content-header-left col-md-4 col-12 mb-2">
            <?php 
              @session_start(); ?>

            <?php 

              if(empty($_SESSION['NOM_MATERIA'])){
             ?>
              <h3 class="content-header-title"></h3>
           <?php } else{ ?>
              <h3 class="content-header-title">Registro de notas de la materia <?php echo $_SESSION['NOM_MATERIA'];?> -Periodo <?php echo $_SESSION['NUM_PER'];?>  </h3>

            <?php  
          } ?>
          </div>



          <div class="content-header-right col-md-8 col-12">

            <div class="breadcrumbs-top float-md-right">
              <div class="breadcrumb-wrapper mr-1">
 
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- Line Awesome section start -->
<section id="line-awesome-icons">
  <div class="row">
      <div class="col-12">
        <!--Aqui inicia la tarjeta --->
          <div class="card">

           <?php 
              @session_start(); ?>

            <?php 

              if(empty($_SESSION['NOM_MATERIA'])){
             ?>
                     <div class="card-header">
                      <h4 class="card-title">Información</h4>
                      <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                      <div class="heading-elements">
                        <ul class="list-inline mb-0">
                          <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                          <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                          <li><a data-action="close"><i class="ft-x"></i></a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="card-content collapse show">
                      <div class="card-body">
                        <h6>Para agregar notas a los alumnos de una materia</h4>
                          1-Clic en la opción Grados del menú lateral.<br>
                          2-Seleccione un grado dando clic en el botón color amarillo (de la columna Materias por grado) de la tabla de la izquierda.<br>
                          3-Seleccione una materia correspondiente al grado seleccionado en la tabla de la derecha.<br>
                      </div>
                    </div>
           <?php } else{ ?>
                  <div class="card-header">
                    <h4 class="card-title">Alumnos</h4>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                      <ul class="list-inline mb-0">
                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="card-content collapse show">
                    <div class="card-body">
                                 <div class="table-responsive" id="div_alumnosxgrado">
                                          <table class="table table-hover table-condensed table-bordered" id="tbl_alumnosxgrado">
                                            <thead style="background-color: #2980b9 ; color:white; font-weight: bold;">
                                              <tr>
                                                <td style="text-align: center;">Apellidos</td>
                                                <td style="text-align: center;">Nombres</td>
                                                <td style="text-align: center;">NIE</td>
                                                <td style="text-align: center;"></td>
                                              </tr>
                                            </thead>
                                            <tbody id="cuerpotablaalumnos">
                                              
                                            </tbody>                       
                                          </table>
                                </div>

                    </div>
                  </div>

            <?php  
          } ?>



          </div> <!--Aqui finaliza la tarjeta -->

      </div>
  </div>
</section>
            <!-- MODAL PARA EDITAR UNA AREA -->
            <div id="addNotasModal" class="modal fade">
              <div class="modal-dialog">
                <div class="modal-content">                 
                    <div class="modal-header">            
                      <h4 class="modal-title">Agregar Notas a  alumno</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                      <div id="controlCantN">
                        
                      </div>

                      <form id="frmAddNotasAlum">
                        <div id="cuerpodelmodaladdNotas">
                          
                        </div>
                      </form>                              
                    </div>
                    <div class="modal-footer">
                      <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                      <input type="submit" id="btnagregarnotas" class="btn btn-info" value="Agregar">
                    </div>                  
                </div>
              </div>
            </div>
<!-- // Line Awesome section end -->
        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-light navbar-border navbar-shadow">

    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="theme-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CHAMELEON  JS-->
    <script src="theme-assets/js/core/app-menu-lite.js" type="text/javascript"></script>
    <script src="theme-assets/js/core/app-lite.js" type="text/javascript"></script>
    <!-- END CHAMELEON  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
        <script type="text/javascript" src="librerias/jquery.min.js"></script>
<script type="text/javascript" src="librerias/bootstrap/popper.min.js"></script>

<script type="text/javascript" src="librerias/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="librerias/datatable/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="librerias/alertify/alertify.js"></script>

<script type="text/javascript">
    $(document).ready(function(){
       var variable = <?php echo json_encode($_SESSION['ID_GRADO']); ?>;
       var IDgrado=<?php echo json_encode($_SESSION['ID_GRADO']); ?>;
    if(IDgrado==""){
    }else{
        listar_alumnosxgrado();
        addnotas();
    }
       


  });


  var listar_alumnosxgrado=function(){
    var idGrado=<?php echo json_encode($_SESSION['ID_GRADO']); ?>;
    $.ajax({
        type:"POST",
        data:"id_grado="+idGrado,
        url:"procesos_notas/obtenAlumxGra.php",
        success:function(datas){
          var midata=JSON.parse(datas);
          if ($.fn.DataTable.isDataTable("#tbl_alumnosxgrado")) {
               $('#tbl_alumnosxgrado').DataTable().clear().destroy();
          }
          let res= document.querySelector('#cuerpotablaalumnos');
          res.innerHTML='';

            for(let item of midata){
              //console.log(item.materia);
              //Aqui tengo que MANDAR TAMBIEN LA MATRICULA

              res.innerHTML+=`
                <tr>
                  <td>${item.apellido_alum}</td>
                  <td>${item.nombre_alum}</td>
                  <td>${item.nie}</td>
                  <td><button type='button' class='btn per1 btn-secondary btn-sm' data-toggle='modal' data-target='#addNotasModal' onclick='f_addNotas(${item.id_alumno})''>Ingresar Notas</button></td>
                </tr>
              `
            }
                      //------
            $('#tbl_alumnosxgrado').DataTable({
              destroy:true,
                  language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                
            });
            //-----



        }

    });

  }
</script>

<script type="text/javascript">
  function f_addNotas(id_alumno){
    //---AQUI esta el proceso central

                 $.ajax({
                  type:"POST",
                  data:"idAlumno=" + id_alumno,
                  url: "procesos_notas/validarAxG.php",
                  success:function(r){
                    datos=jQuery.parseJSON(r);
                    if(datos['result_consulta']==0){
                      alert("El alumno no corresponde con la materia seleccionada");
                    }else{
                      //alertify.success("El alumno seleccionado si corresponde con la materia seleccionada");
                      console.log(id_alumno);
                              ///Traer las evaluaciones correspondientes al periodo
                              var idGrado=<?php echo json_encode($_SESSION['ID_GRADO']); ?>;
                              $.ajax({
                                type:"POST",
                                data:"id_grado="+idGrado,
                                url:"procesos_notas/obtenerEvaxPerxAlum.php",
                                success:function(r2){
                                  ///Vamos a crear los controles en el modal de forma dinamica :)
                                  var misEva=JSON.parse(r2);
                                  var cantidadEva=misEva.length;

                                  let res= document.querySelector('#cuerpodelmodaladdNotas');
                                  let res2=document.querySelector('#controlCantN');
                                  res2.innerHTML='';
                                  res.innerHTML='';
                                  res2.innerHTML=`<div class='form-group'>
                                  <input type='text' id="cantEva" name="cantEva" class="form-control" hidden="true" value="${cantidadEva}" >
                                  </div> 
                                  <div class='form-group'>
                                  <input type='text' id="idAlum" name="idAlum" class="form-control" hidden="true" value="${id_alumno}" >
                                  </div> 
                                  `;
                                  var contEva;
                                  contEva=1;
                                  for(let item of misEva){
                                  //console.log(item.materia);
                                  //Aqui tengo que MANDAR TAMBIEN LA MATRICULA
                                  res.innerHTML+=`
                                  <div class='form-group'>
                                  <label>${item.nombre_evaluacion}</label>
                                  <input type='number' min='0' max='10' id="${item.id_evaluacion}" name="eva${item.id_evaluacion}" class="form-control"  required>
                                  </div> 
                                  `
                                  contEva+=1;
                                }
                              }
                            });
                              ////FIN de traer las evaluaciones correspondientes al periodo         
                            }
                          }
                        });
         //----FINALIZA PROCESO CENTRAL


        












  }

</script>
<script type="text/javascript">
      //Funcion para agregar una nueva area
    var addnotas= function(){
      $('#btnagregarnotas').click(function(){
        var cantEva=$("#cantEva").val();
        var idAlum=$("#idAlum").val();
        //console.log(cantEva);

            var campos, validoCamposVacios,validoCamposNegativos;

            // todos los campos .form-control en #campos
            campos = document.querySelectorAll('#cuerpodelmodaladdNotas input.form-control');
            validoCamposVacios = true; // es valido hasta demostrar lo contrario
            validoCamposNegativos=true;
            validoRangoNota=true;
            // recorremos todos los campos
            [].slice.call(campos).forEach(function(campo) {
                //console.log(campo.value.trim());
                // el campo esta vacio?
                console.log(campo.id.trim());
                if (campo.value.trim() === '') {
                    validoCamposVacios = false;
                }
                if (parseInt(campo.value) <0) {
                    validoCamposNegativos = false;
                }

                 if (parseInt(campo.value)<0 || parseInt(campo.value)>10) {
                    validoRangoNota = false;
                }
            });

            //-------------
                            $.ajax ({
                            type:"POST",
                            data:"idAlumno="+idAlum,
                            url:"procesos_notas/validarExisNotasAlumSelec.php",
                            success:function(rr){
                               data3=jQuery.parseJSON(rr);
                              if (data3['result_consulta']==0) {
                                  if (validoCamposVacios) {
                                  // es valido! procedemos?
                                      if (validoCamposNegativos) {

                                          if(validoRangoNota){
                                            //console.log("TODO BIEN");
                                            //PROCESO DE INGRESAR NOTAS
                                            //PASO 1: Obtener el id de la matricula del alumno seleccionado
                                            $.ajax({
                                              type:"POST",
                                              data:"idAlumno="+idAlum,
                                              url:"procesos_notas/obtenerIdMatriculaXAlumno.php",
                                              success:function(mm){
                                                var midataIdMatricula=JSON.parse(mm);
                                                //console.log(midataIdMatricula);
                                                var miIdMatricula=midataIdMatricula[0].id_matricula;
                                                //console.log(parseInt(miIdMatricula));
                                                //alertify.success("Mi id matricula "+parseInt(miIdMatricula));
                                                ingresarNotasPaso2(miIdMatricula);
                                              }
                                            });
                                            //FIN DE PROCESO INGRESAR NOTAS
                                          }else{
                                            alertify.error("La notas deben estar entre 0 y 10");
                                          }
                                      } else {
                                           alertify.error("No se permite ingresar valores negativos");
                                      }
                                  } else {
                                      alertify.error("Debe llenar todos los campos");
                                  }
                              }else{ 
                              alertify.warning("Ya se ha asigando notas previamente al alumno seleccionado");
                            }

                            }
                          });
          //--------




    });

    }



          var ingresarNotasPaso2=function(idMatricula){
            var campos;

            // todos los campos .form-control en #campos
            campos = document.querySelectorAll('#cuerpodelmodaladdNotas input.form-control');
            // recorremos todos los campos
            [].slice.call(campos).forEach(function(campo) {
                console.log(campo.id.trim());
                console.log(campo.value.trim());
                ingresarNotasPaso3(campo.id.trim(),campo.value.trim(),idMatricula);
            });
            alertify.success("¡Notas agregadas con exito!");
          
         }

         var ingresarNotasPaso3=function(idEvaluacion,nota,idMatricula){
            $.ajax({
                  type:"POST",
                  data:{"idEvaluacion":idEvaluacion,"nota":nota,"idMatricula":idMatricula},
                  url: "procesos_notas/agregarNotaAlumno.php",
                  success:function(r){
                    if(r==1){
                    }else{
                      alertify.error("¡Fallo al agregar la nota!");
                    }

                  }
              });
         }




</script>
  </body>
</html>