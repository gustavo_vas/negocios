-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-05-26 07:32:53.607

-- tables
-- Table: Alumno
CREATE TABLE Alumno (
    id_alumno int NOT NULL AUTO_INCREMENT,
    nie char(15) NOT NULL,
    nombre_alum varchar(30) NOT NULL,
    apellido_alum varchar(30) NOT NULL,
    direccion_alum varchar(80) NOT NULL,
    telefono_alum varchar(9) NOT NULL,
    id_usuario int NOT NULL,
    CONSTRAINT Alumno_pk PRIMARY KEY (id_alumno)
);

-- Table: Grado
CREATE TABLE Grado (
    id_grado int NOT NULL AUTO_INCREMENT,
    grado varchar(40) NOT NULL,
    CONSTRAINT Grado_pk PRIMARY KEY (id_grado)
);

-- Table: HistorialNotas
CREATE TABLE HistorialNotas (
    id_historial int NOT NULL AUTO_INCREMENT,
    id_nota int NOT NULL,
    id_matricula int NOT NULL,
    CONSTRAINT HistorialNotas_pk PRIMARY KEY (id_historial)
);

-- Table: Materia
CREATE TABLE Materia (
    id_materia int NOT NULL AUTO_INCREMENT,
    materia varchar(30) NOT NULL,
    CONSTRAINT Materia_pk PRIMARY KEY (id_materia)
);

-- Table: Nota
CREATE TABLE Nota (
    id_nota int NOT NULL AUTO_INCREMENT,
    id_evaluacion int NOT NULL,
    nota double(9,4) NOT NULL,
    CONSTRAINT Nota_pk PRIMARY KEY (id_nota)
);

-- Table: Periodos
CREATE TABLE Periodos (
    id_periodo int NOT NULL AUTO_INCREMENT,
    periodo int NOT NULL,
    CONSTRAINT Periodos_pk PRIMARY KEY (id_periodo)
);

-- Table: Profesor
CREATE TABLE Profesor (
    id_profesor int NOT NULL AUTO_INCREMENT,
    nombre varchar(20) NOT NULL,
    apellido varchar(20) NOT NULL,
    direccion varchar(80) NOT NULL,
    telefono varchar(9) NOT NULL,
    id_usuario int NOT NULL,
    CONSTRAINT Profesor_pk PRIMARY KEY (id_profesor)
);

-- Table: evaluaciones
CREATE TABLE evaluaciones (
    id_evaluacion int NOT NULL AUTO_INCREMENT,
    id_profXmat int NOT NULL,
    id_periodo int NOT NULL,
    nombre_evaluacion varchar(20) NOT NULL,
    porcentaje double(9,4) NOT NULL,
    CONSTRAINT evaluaciones_pk PRIMARY KEY (id_evaluacion)
);

-- Table: graXproXmat
CREATE TABLE graXproXmat (
    id_graXproXmat int NOT NULL AUTO_INCREMENT,
    id_profXmat int NOT NULL,
    id_grado int NOT NULL,
    CONSTRAINT graXproXmat_pk PRIMARY KEY (id_graXproXmat)
);

-- Table: login
CREATE TABLE login (
    id_usuario int NOT NULL AUTO_INCREMENT,
    usuario varchar(100) NOT NULL,
    contrasena varchar(100) NOT NULL,
    id_tipousuartio int NOT NULL,
    CONSTRAINT login_pk PRIMARY KEY (id_usuario)
) COMMENT 'tabla para el login';

-- Table: matricula
CREATE TABLE matricula (
    id_matricula int NOT NULL AUTO_INCREMENT,
    id_alumno int NOT NULL,
    id_grado int NOT NULL,
    fecha_matricula date NOT NULL,
    CONSTRAINT matricula_pk PRIMARY KEY (id_matricula)
);

-- Table: profXmat
CREATE TABLE profXmat (
    id_profXmat int NOT NULL AUTO_INCREMENT,
    id_profesor int NOT NULL,
    id_materia int NOT NULL,
    CONSTRAINT profXmat_pk PRIMARY KEY (id_profXmat)
);

-- Table: tipousuario
CREATE TABLE tipousuario (
    id_tipousuartio int NOT NULL AUTO_INCREMENT,
    tipo_usuario varchar(100) NOT NULL,
    CONSTRAINT tipousuario_pk PRIMARY KEY (id_tipousuartio)
) COMMENT 'aloja los tipos de usuario';

-- foreign keys
-- Reference: Alumno_login (table: Alumno)
ALTER TABLE Alumno ADD CONSTRAINT Alumno_login FOREIGN KEY Alumno_login (id_usuario)
    REFERENCES login (id_usuario);

-- Reference: HistorialNotas_Nota (table: HistorialNotas)
ALTER TABLE HistorialNotas ADD CONSTRAINT HistorialNotas_Nota FOREIGN KEY HistorialNotas_Nota (id_nota)
    REFERENCES Nota (id_nota);

-- Reference: HistorialNotas_grupXalum (table: HistorialNotas)
ALTER TABLE HistorialNotas ADD CONSTRAINT HistorialNotas_grupXalum FOREIGN KEY HistorialNotas_grupXalum (id_matricula)
    REFERENCES matricula (id_matricula);

-- Reference: Matricula_profXmat (table: graXproXmat)
ALTER TABLE graXproXmat ADD CONSTRAINT Matricula_profXmat FOREIGN KEY Matricula_profXmat (id_profXmat)
    REFERENCES profXmat (id_profXmat);

-- Reference: Nota_evaluaciones (table: Nota)
ALTER TABLE Nota ADD CONSTRAINT Nota_evaluaciones FOREIGN KEY Nota_evaluaciones (id_evaluacion)
    REFERENCES evaluaciones (id_evaluacion);

-- Reference: Profesor_login (table: Profesor)
ALTER TABLE Profesor ADD CONSTRAINT Profesor_login FOREIGN KEY Profesor_login (id_usuario)
    REFERENCES login (id_usuario);

-- Reference: evaluaciones_Periodos (table: evaluaciones)
ALTER TABLE evaluaciones ADD CONSTRAINT evaluaciones_Periodos FOREIGN KEY evaluaciones_Periodos (id_periodo)
    REFERENCES Periodos (id_periodo);

-- Reference: evaluaciones_profXmat (table: evaluaciones)
ALTER TABLE evaluaciones ADD CONSTRAINT evaluaciones_profXmat FOREIGN KEY evaluaciones_profXmat (id_profXmat)
    REFERENCES profXmat (id_profXmat);

-- Reference: graXproXmat_Grado (table: graXproXmat)
ALTER TABLE graXproXmat ADD CONSTRAINT graXproXmat_Grado FOREIGN KEY graXproXmat_Grado (id_grado)
    REFERENCES Grado (id_grado);

-- Reference: grupXalum_Alumno (table: matricula)
ALTER TABLE matricula ADD CONSTRAINT grupXalum_Alumno FOREIGN KEY grupXalum_Alumno (id_alumno)
    REFERENCES Alumno (id_alumno);

-- Reference: login_tipousuario (table: login)
ALTER TABLE login ADD CONSTRAINT login_tipousuario FOREIGN KEY login_tipousuario (id_tipousuartio)
    REFERENCES tipousuario (id_tipousuartio);

-- Reference: matricula_Grado (table: matricula)
ALTER TABLE matricula ADD CONSTRAINT matricula_Grado FOREIGN KEY matricula_Grado (id_grado)
    REFERENCES Grado (id_grado);

-- Reference: profXmat_Materia (table: profXmat)
ALTER TABLE profXmat ADD CONSTRAINT profXmat_Materia FOREIGN KEY profXmat_Materia (id_materia)
    REFERENCES Materia (id_materia);

-- Reference: profXmat_Profesor (table: profXmat)
ALTER TABLE profXmat ADD CONSTRAINT profXmat_Profesor FOREIGN KEY profXmat_Profesor (id_profesor)
    REFERENCES Profesor (id_profesor);

-- End of file.

