
<form action="secure_login.php" method='post' id="bb" class="login100-form validate-form">
	<span class="login100-form-title p-b-26">
		Bienvenidos
	</span>
	<span class="login100-form-title p-b-48">						
		<picture>
			<img src="images/logo1.png" alt="logo" width="50%" >
		</picture>
	</span>
	<div class="wrap-input100 validate-input" data-validate = "Ingrese su Usuario">
		<input class="input100" type="text" name="user_id_auth" id="textfield" autocomplete="off">
		<span class="focus-input100" data-placeholder="Usuario"></span>
	</div>
	<div class="wrap-input100 validate-input" data-validate="Ingrese su Clave">
		<span class="btn-show-pass">
			<i class="zmdi zmdi-eye"></i>
		</span>
		<input class="input100" type="password" name="pass_key" id="pwfield">
		<span class="focus-input100" data-placeholder="Password"></span>
	</div>
	<div class="container-login100-form-btn">
		<div class="wrap-login100-form-btn">
			<div class="login100-form-bgbtn"></div>
			<button type="submit" name="btnLogin" class="login100-form-btn">
				Iniciar Sesion
			</button>
		</div>
	</div><br>
</form>
