$(document).ready(function(){
    buscar_datos();
    cargarlogin();
    $.ajax({
        type: 'POST',
        url: 'Modelos/cargar_grado.php'
    })
    .done(function(lista_grado){
        $('#Sgrado').html(lista_grado)
    })
    .fail(function(){
        alert('Error al Cargar los Datos')
    })

    $('#Sgrado').on('change', function(){
        var idG = $('#Sgrado').val()
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_alumXgrad.php',
            data :{'idG' : idG }
        })
        .done(function(lista_seccion){
            $('#Tag').html(lista_seccion)
        })
        .fail(function(){
          alert('Error al Cargar las Secciones')
        })
    })

    $('#btnAggAlumno').click(function(){
        var nomA = $('#nomalum').val()
        var nieA = $('#niealum').val()
        var apeA = $('#apellalum').val()
        var dirA = $('#direcalum').val()
        var telA = $('#telalum').val()
        if(nomA.length==0 || nieA.length==0 || apeA.length==0 || dirA.length==0 || telA.length==0){
            alertify.error("Campos Vacios");
        }else{
            $.ajax({
                type: 'POST',
                url: 'Modelos/insert_alumno.php',
                data :{'nomA' : nomA, 'apeA' : apeA, 'dirA' : dirA, 'telA' : telA,'nieA' : nieA}
            })
            .done(function(lista_seccionT){
                alertify.success("Datos Almacenados con Exito");
                buscar_datos();
            })
            .fail(function(){
                alert('Error al Cargar las secciones en la tablas')
            })
        }
    })

    $('#btnEditAlumno').click(function(){
        
        var EidA = $('#Eidalum').val()
        var EnomA = $('#Enomalum').val()
        var EnieA = $('#Eniealum').val()
        var EapeA = $('#Eapellalum').val()
        var EdirA = $('#Edirecalum').val()
        var EtelA = $('#Etelalum').val()
        var EestA = $('#Eestaalum').val()
        var ElogA = $('#Elogalum').val()
        Elogalum
        $.ajax({
            type: 'POST',
            url: 'Modelos/update_alumno.php',
            data :{'EidA' : EidA, 'EnomA' : EnomA, 'EnieA' : EnieA, 'EapeA' : EapeA,'EdirA' : EdirA
            ,'EtelA' : EtelA,'EestA' : EestA,'ElogA' : ElogA}
        })
        .done(function(lista_seccionT){
            buscar_datos();
        })
        .fail(function(){
          alert('Error al Cargar las secciones en la tablas')
        })
    })

    function buscar_datos(consulta){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_alumnoT1.php',
            dataType: 'html',
            data: {consulta: consulta}
        })
        .done(function(respuesta){
            $('#Talum').html(respuesta)
        })
        .fail(function(){
            alert('Error al Cargar alumno')
        })
    }

    $(document).on('keyup', '#caja_busqueda', function(){
        var valor=$(this).val();
        if(valor != ""){
            buscar_datos(valor);
        }else{
            buscar_datos();
        }
    })

   

    function cargarlogin(){
        $.ajax({
            type: 'POST',
            url: 'Modelos/cargar_login.php'
        })
        .done(function(lista_alumno){
            $('#Elogalum').html(lista_alumno)
        })
        .fail(function(){
            alert('Error al Cargar los Maestros en el select')
        })
    }

    

})

function editAlum(datos){
    
    d=datos.split("||");
    
    
    $('#Eidalum').val(d[0]);
    $('#Eniealum').val(d[1]);
    $('#Enomalum').val(d[2]);
    $('#Eapellalum').val(d[3]);
    $('#Edirecalum').val(d[4]);
    $('#Etelalum').val(d[5]);
    $('#Eestaalum').val(d[6]);
    $('#Elogalum').val(d[7]);
}