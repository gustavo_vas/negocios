<?php
  require_once '../../config/conexion.php';
  $conec=new conexion();
  $con=$conec->conectar();
  $conec->page_protect();
?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>    
    <title>Inicio</title>
  <?php include("estructura/enhead.php"); ?>
  </head>
  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click"data-menu="vertical-menu" data-color="bg-chartbg" data-col="2-columns">
    <!-- datos del usuario ingresado -->
    <?php include("estructura/cabecera.php"); ?>
    <!-- muestra el menu de navegacion -->
    <?php include("estructura/menu.php"); ?>

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"><hr></div>
        <div class="content-header row">
          <hr>
        </div>
        <div class="content-body">
          <!-- eCommerce statistic -->
          <div class="row">
              <div class="col-xl-4 col-lg-6 col-md-12">
                <a href="notas.php">
                  <div class="card pull-up ecom-card-1 bg-white">
                      <div class="card-content ecom-card2 height-180">
                          <h5 class="text-muted danger position-absolute p-1">NOTAS</h5>
                          <div>
                              <i class="ft-pie-chart danger font-large-1 float-right p-1"></i>
                          </div>
                          <div class="progress-stats-container ct-golden-section height-75 position-relative pt-3  ">
                              <!-- <div id="progress-stats-bar-chart"></div> -->
                              <div id="progress-stats-line-chart" class="progress-stats-shadow"></div>
                          </div>
                      </div>
                  </div>
                  </a>
              </div>
              <div class="col-xl-4 col-lg-6 col-md-12">
                  <div class="card pull-up ecom-card-1 bg-white">
                      <div class="card-content ecom-card2 height-180">
                          <h5 class="text-muted info position-absolute p-1">EXPEDIENTE DE NOTAS</h5>
                          <div>
                              <i class="ft-activity info font-large-1 float-right p-1"></i>
                          </div>
                          <div class="progress-stats-container ct-golden-section height-75 position-relative pt-3">
                              <!-- <div id="progress-stats-bar-chart1"></div> -->
                              <div id="progress-stats-line-chart1" class="progress-stats-shadow"></div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-xl-4 col-lg-12">
                <a href="miperfil.php">
                  <div class="card pull-up ecom-card-1 bg-white">
                      <div class="card-content ecom-card2 height-180">
                          <h5 class="text-muted info position-absolute p-1">PERFIL</h5>
                          <div>
                              <i class="ft-user info font-large-1 float-right p-1"></i>
                          </div>
                          <div class="progress-stats-container ct-golden-section height-75 position-relative pt-3">
                              <!-- <img src="theme-assets/images/portrait/small/img.png"> -->
                          </div>
                      </div>
                  </div>
                  </a>
              </div>
          </div>
        </div>
      </div>
    </div>
    <?php include("estructura/script.php"); ?>
  </body>
</html>