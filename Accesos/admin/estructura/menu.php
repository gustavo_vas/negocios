<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="../theme-assets/images/backgrounds/02.jpg">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">       
          <li class="nav-item mr-auto"><a class="navbar-brand" href="index.php"><img class="brand-logo" alt="Chameleon admin logo" src="../theme-assets/images/logo/logo.png"/>
              <h3 class="brand-text">Admin</h3></a></li>
          <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
      </div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        <li class=" nav-item"><a href="index.php"><i class="ft-home"></i><span class="menu-title" data-i18n="">Inicio</span></a>
          </li>
          <li class=" nav-item"><a href="alumnos.php"><i class="la la-street-view"></i><span class="menu-title" data-i18n="">Alumnos</span></a>
          </li>
          <li class=" nav-item"><a href="maestros.php"><i class="la la-user-plus"></i><span class="menu-title" data-i18n="">Maestros</span></a>
          </li>
          <li class=" nav-item"><a href="grados.php"><i class="la la-institution"></i><span class="menu-title" data-i18n="">Grado</span></a>
          </li>
          <li class=" nav-item"><a href="materia.php"><i class="la la-book"></i><span class="menu-title" data-i18n="">Materia</span></a>
          </li>
          <li class=" nav-item"><a href="matricular.php"><i class="la la-paste"></i><span class="menu-title" data-i18n="">Matricular</span></a>
          </li>
          <li class=" nav-item"><a href="logout.php"><i class="la la-paste"></i><span class="menu-title" data-i18n="">Cerrar Sesion</span></a>
          </li>
        </ul>
      </div>
    </div>