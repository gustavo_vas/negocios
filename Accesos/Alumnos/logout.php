<?php
session_start();
session_destroy();
date_default_timezone_set('America/El_Salvador');
header("Location: ../index.php");
?>