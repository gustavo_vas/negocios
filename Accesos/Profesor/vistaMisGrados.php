  <?php
   session_start();
  
  $_SESSION['ID_GRADO']  = "";
  $_SESSION['ID_MAT'] = "";
  $_SESSION['NUM_PER'] = ""; 
  $_SESSION['NOM_MATERIA'] ="";
  $_SESSION['ID_PROFXMAT'] ="";
  ?>
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Chameleon Admin is a modern Bootstrap 4 webapp &amp; admin dashboard html template with a large number of components, elegant design, clean and organized code.">
    <meta name="keywords" content="admin template, Chameleon admin template, dashboard template, gradient admin template, responsive admin template, webapp, eCommerce dashboard, analytic dashboard">
    <meta name="author" content="ThemeSelect">
    <title>Portal del Maestro</title>
    <link rel="apple-touch-icon" href="theme-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="theme-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,300i,400,400i,600,600i,700,700i%7CComfortaa:300,400,700" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/vendors.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN CHAMELEON  CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/app-lite.css">
    <!-- END CHAMELEON  CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="theme-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="theme-assets/css/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <!-- END Custom CSS-->
     <link rel="stylesheet" type="text/css" href="librerias/datatable/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" type="text/css" href="librerias/alertify/css/alertify.css">
  <link rel="stylesheet" type="text/css" href="librerias/alertify/css/themes/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="librerias/fontawesome/css/font-awesome.css">
  </head>
  <body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar" data-open="click" data-menu="vertical-menu" data-color="bg-gradient-x-purple-blue" data-col="2-columns">

    <!-- fixed-top-->
    <nav class="header-navbar navbar-expand-md navbar navbar-with-menu navbar-without-dd-arrow fixed-top navbar-semi-light">
      <div class="navbar-wrapper">
        <div class="navbar-container content">
          <div class="collapse navbar-collapse show" id="navbar-mobile">
            <ul class="nav navbar-nav mr-auto float-left">
              <li class="nav-item d-block d-md-none"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu"></i></a></li>
              <li class="nav-item d-none d-md-block"><a class="nav-link nav-link-expand" href="#"><i class="ficon ft-maximize"></i></a></li>
              <li class="nav-item dropdown navbar-search"><a class="nav-link dropdown-toggle hide" data-toggle="dropdown" href="#"><i class="ficon ft-search"></i></a>
                <ul class="dropdown-menu">
                  <li class="arrow_box">
                    <form>
                      <div class="input-group search-box">
                        <div class="position-relative has-icon-right full-width">
                          <input class="form-control" id="search" type="text" placeholder="Search here...">
                          <div class="form-control-position navbar-search-close"><i class="ft-x">   </i></div>
                        </div>
                      </div>
                    </form>
                  </li>
                </ul>
              </li>
            </ul>

          </div>
        </div>
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true" data-img="theme-assets/images/backgrounds/02.jpg">
      <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">       
          <li class="nav-item mr-auto"><a class="navbar-brand" ><img class="brand-logo" alt="Chameleon admin logo" src="theme-assets/images/logo/logo.png"/>
              <h3 class="brand-text">Chameleon</h3></a></li>
          <li class="nav-item d-md-none"><a class="nav-link close-navbar"><i class="ft-x"></i></a></li>
        </ul>
      </div>
      <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
         <li class=" nav-item"><a href="vistaMisMaterias.php"><i class="ft-home"></i><span class="menu-title" data-i18n="">Materias</span></a>
          </li>
          <li class=" nav-item"><a href="vistaMisGrados.php"><i class="ft-home"></i><span class="menu-title" data-i18n="">Grados</span></a>
          </li>

        </ul>
      </div>
    </div>

    <div class="app-content content">
      <div class="content-wrapper">
        <div class="content-wrapper-before"></div>
        <div class="content-header row">
          <div class="content-header-left col-md-4 col-12 mb-2">
            <h3 class="content-header-title">Mis grados asignados</h3>
          </div>
          <div class="content-header-right col-md-8 col-12">
            <div class="breadcrumbs-top float-md-right">
              <div class="breadcrumb-wrapper mr-1">
               
              </div>
            </div>
          </div>
        </div>
        <div class="content-body"><!-- ../../../theme-assets/images/carousel/22.jpg -->



<!-- Content types section start -->
<section id="content-types">

	<div class="row match-height">

		<div class="col-md-6 col-sm-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<h4 class="card-title">Grados</h4>
						<p class="card-text"></p>
					</div>
					<div class="card-body">
						           <!-- --> 
           <div class="table-responsive" id="misGrados">
                    <table class="table table-hover table-condensed table-bordered" id="miTablaGrados">
                      <thead style="background-color: #2980b9 ; color:white; font-weight: bold;">
                        <tr>
                          <td style="width:50px">Id Grado</td>
                          <td style="width:200px">Grado</td>
                          <td style="text-align: center;">Materias por Grado</td>
                        </tr>
                      </thead>                       
                    </table>
          </div>
           <!-- /////////////////// --->
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6 col-sm-12">
			<div class="card">
				<div class="card-content">
					<div class="card-body">
						<h4 class="card-title">Materias por grado</h4>
						
					</div>
					<div class="card-body">
					                      <!-- --> 
			           <div class="table-responsive" id="misMatxGradoxPro">
			                    <table class="table table-hover table-condensed table-bordered" id="miTablaMatxGradXPro">
			                      <thead style="background-color: #2980b9 ; color:white; font-weight: bold;">
			                        <tr>
			                          <td >Materia</td>
			                          <td style="width:200px">Periodos</td>
                                <td style="width:50px">Notas Globales</td>
			                        </tr>
			                      </thead>
			                      <tbody id="cuerpoMitabla2">
			                      	
			                      </tbody>                       
			                    </table>
			          </div>
           <!-- /////////////////// --->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Content types section end -->



        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-light navbar-border navbar-shadow">

    </footer>

    <!-- BEGIN VENDOR JS-->
    <script src="theme-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN CHAMELEON  JS-->
    <script src="theme-assets/js/core/app-menu-lite.js" type="text/javascript"></script>
    <script src="theme-assets/js/core/app-lite.js" type="text/javascript"></script>
    <!-- END CHAMELEON  JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <!-- END PAGE LEVEL JS-->
    <script type="text/javascript" src="librerias/jquery.min.js"></script>
<script type="text/javascript" src="librerias/bootstrap/popper.min.js"></script>

<script type="text/javascript" src="librerias/datatable/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="librerias/datatable/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="librerias/alertify/alertify.js"></script>




<script type="text/javascript">
  /*$(document).ready(function(){
    var variable = <?php //echo json_encode($_SESSION['mi_id_profesor']); ?>;
       alert(variable);

  });*/
  $(document).ready(function(){
     $("#miTablaGrados").dataTable().fnDestroy();
    listarMisGrados();



  });

  // $(document).on("ready",function(){
  //   listarMisGrados();
  // });

  var listarMisGrados=function(){
    var table=$("#miTablaGrados").DataTable({
      "destroy":true,
        "ajax":{
                "method":"POST",
                "url":"procesos_grados/obtenDatos_grados.php"
              },
              "columns":[
                {"data":"id_grado"},
                {"data":"grado"},
                {"defaultContent":"<button type='button' class='btn editar btn-warning btn-sm' ><span class='fa fa-book'></span></button>"}
              ],
              "language":idioma_espanol
        });
    obtener_data_consultar("#miTablaGrados tbody",table);
  }

  var obtener_data_consultar=function(tbody,table){
    $(tbody).on("click","button.editar",function(e){
      var data = table.row($(this).parents("tr")).data();
      //console.log(data);
      var id_grado= data.id_grado;
      // $("#miTablaMatxGradXPro").dataTable().fnClearTable();

      console.log(id_grado);
      listarMisMateriasxMiGrado(id_grado);

    });
  }

  var listarMisMateriasxMiGrado=function(idGrado){
     //console.log(idGrado);

    $.ajax({
      type:"POST",
      data:"id_grado=" + idGrado,
      url: "procesos_grados/validacionGradoxProf.php",
      success:function(r){
        datos=jQuery.parseJSON(r);
        //Aqui le estamos asigando valores a los input en el modal actualizar
        if(datos['result_consulta']==0){
          alert("La materia no corresponde con el profesor logueado");
          console.log(idGrado);
        }else{
          //alert("La Materia si corresponde con el profesor logueado");
          console.log(idGrado);

			$.ajax({
				type:"POST",
				data:"id_grado="+idGrado,
				url:"procesos_grados/obtenMxGxP.php",
				success:function(datas){
					//var filas= datas.length;
					var midata=JSON.parse(datas);
					var filas= midata.length;
					//console.log(midata);
					//console.log(filas);
					var minombre= midata[0].id_materia;
	          if ($.fn.DataTable.isDataTable("#miTablaMatxGradXPro")) {
               $('#miTablaMatxGradXPro').DataTable().clear().destroy();
           		}

						let res= document.querySelector('#cuerpoMitabla2');
						res.innerHTML='';

						for(let item of midata){
							//console.log(item.materia);
							res.innerHTML+=`
								<tr>
									<td>${item.materia}</td>
									<td><button type='button' class='btn per1 btn-secondary btn-sm' onclick='f_addnotasper1(${item.id_materia},1)''>1</button>&nbsp;&nbsp;<button type='button' class='btn per2 btn-primary btn-sm' onclick='f_addnotasper2(${item.id_materia},2)'>2</button>&nbsp;&nbsp;<button type='button' class='btn per2 btn-info btn-sm'  onclick='f_addnotasper3(${item.id_materia},3)'>3</button>&nbsp;&nbsp;<button type='button' class='btn per2 btn-danger btn-sm' onclick='f_addnotasper4(${item.id_materia},4)'>4</button></td>
                    <td>&nbsp;<button type='button' class='btn per2 btn-danger btn-sm' onclick='f_notasGlobales(${item.id_materia})'>N</button></td>
								</tr>
							`
						}
					//------
						$('#miTablaMatxGradXPro').DataTable({
							destroy:true,
									language: {
						        "decimal": "",
						        "emptyTable": "No hay información",
						        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
						        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
						        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
						        "infoPostFix": "",
						        "thousands": ",",
						        "lengthMenu": "Mostrar _MENU_ Entradas",
						        "loadingRecords": "Cargando...",
						        "processing": "Procesando...",
						        "search": "Buscar:",
						        "zeroRecords": "Sin resultados encontrados",
						        "paginate": {
						            "first": "Primero",
						            "last": "Ultimo",
						            "next": "Siguiente",
						            "previous": "Anterior"
						        }
						    },
						    
						});
						//-----

				}//---fin del success del ajax2
			}); ///---fin del ajax2
            ///-------------------------
        }//--Fin del else
      
      }//--fin del success ajax 1
    });//fin del ajax 1
  }//fin de la variable :D

 


  var idioma_espanol={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}

	
</script>

<script type="text/javascript">
	function f_addnotasper1(idMateria,periodo){
		//Primero validaremos que el id de la materia si corresponda al maestro logueado
		procesoNotas(idMateria,periodo);
	}
		function f_addnotasper2(idMateria,periodo){
		//Primero validaremos que el id de la materia si corresponda al maestro logueado
		procesoNotas(idMateria,periodo);
	}
		function f_addnotasper3(idMateria,periodo){
		//Primero validaremos que el id de la materia si corresponda al maestro logueado
		procesoNotas(idMateria,periodo);
	}
		function f_addnotasper4(idMateria,periodo){
		//Primero validaremos que el id de la materia si corresponda al maestro logueado
		procesoNotas(idMateria,periodo);
	}


	var procesoNotas= function(idMateria,periodo){

		$.ajax({
			type:"POST",
			data:"id_materia=" + idMateria,
			url: "procesos_grados/validacionMatxProf.php",
			success:function(r){
				datos=jQuery.parseJSON(r);
				if(datos['result_consulta']==0){
					alertify.error("La materia no corresponde con el profesor logueado");
				}else{
			          	//Valicación para existencia de evaluaciones en dicha materia :D
			          	//---
			          	$.ajax({
			          		type:"POST",
			          		data:{"id_materia":idMateria,"num_per":periodo},
			          		url: "procesos_grados/validacionExisEva.php",
			          		success:function(r2){
			          			datos2=jQuery.parseJSON(r2);
			          			if(datos2['result_consulta']==0){
			          				alertify.error("La materia aun no tiene configurada ninguna evaluación");
			          			}else{
				      				//alertify.notify("La materia si tiene configurada alguna evaluacion");
				      				//Como si tiene configurada alguna evaluacion hay que ver si esta dividido el 100% 

				      				//--Validación cantidad de evaluacion y porcentajes
				      				$.ajax({
				      					type:"POST",
				      					data:{"id_materia":idMateria,"num_per":periodo},
				      					url: "procesos_grados/validacionPorcEva.php",
				      					success:function(r3){
				      						var midata=JSON.parse(r3);
				      						let porcAcu=0.000;

							      			for(let item of midata){
												//console.log(item.porcentaje);
												porcAcu+=parseFloat(item.porcentaje);
											}

											if(porcAcu!=1){
												alertify.error("Se debe utilizar el 100% ");
												//console.log(porcAcu);
											}else{
												var idGradoMat=midata[0].id_grado;
												var NomMateria=midata[0].materia;
                        var idProfXMat=midata[0].id_profXmat;
												alertify.notify("El 100% ha sido dividido correctamente");
												alertify.notify("Grado: "+idGradoMat);
												//console.log(porcAcu);
												//--Ahora sii ya que validamos que todo este correctamente, hay que dar otro paso para asignar notas
												$.ajax({
													type: "POST",
													url:"procesos_grados/puenteAddNotas.php",													
													data: {"id_grado": idGradoMat,"idMat": idMateria,"Per":periodo,"nomMateria":NomMateria,"idProfXMat":idProfXMat},
													success: function(respuesta){
														window.location.href = 'addnotas.php';
													}

												});
												//--
											}

										}
									});
				      				//--
				      			}
				      		}
				      	});
			          	//---
			          }


			      }
			  });
	}




  var f_notasGlobales=function(idMateria){

    $.ajax({
      type:"POST",
      data:"idMateria="+idMateria,
      url: "procesos_grados/obtenerIdProfxMat.php",
      success:function(r){
        var midata=JSON.parse(r);
        var idProfXMat=midata[0].id_profXmat;
        var nombreMat=midata[0].materia;
          $.ajax({
            type: "POST",
            url:"procesos_grados/puenteVerNotas.php",                         
            data: {"idProfXMat":idProfXMat,"nombreMat":nombreMat},
            success: function(respuesta){
              window.location.href = 'vernotas.php';
            }

          });
      }
    });


  }

</script>





  </body>
</html>